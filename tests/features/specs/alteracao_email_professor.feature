@altera_email_professor
Feature: Alteração de email do professor

Background:
    Given que acesso a aba dados pessoais do somosid


@altera_sem_sucesso
Scenario Outline: Alteração de email sem sucesso
    Then eu altero meu "<email_professor>" para um email inválido
    Then valido "<mensagem>" de alteração inválida
    Then valido botão salvar desabilitado

Examples: 
        | email_professor           | mensagem                                                            |
        | prof@                     | E-mail inválido. Exemplo de endereço válido: ariel.silva@gmail.com  |
        | 1231231@qweqweqwe         | E-mail inválido                                                     |                                                           
        | profersso@gmail.com.br    | Domínio inválido                                                    |                                                                  
        | teste@mailinator.com      | Email indisponível para uso! Se este for seu e-mail, faça seu login |    

@altera_com_sucesso
Scenario Outline: Alteração de email do professor com sucesso
    When eu altero meu "<email_professor>" para um email correto
    Then valido "<mensagem>" de sucesso   
#   Then ativo o novo "<email_professor>" e realizo novo login

Examples:
        | email_professor                       | mensagem          |
        | emailnovoprofessor@mailinator.com     | E-mail alterado!  |  

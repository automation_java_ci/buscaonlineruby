@somosidinterfaces @somosidglobal @somosidautomatizado @somosidautomatizado
Feature: Usuários ativos
    # O sistema deve permitir que os usuários ativos possam alterar seus dados, acrescentar novos vouchers e redefinir sua senha.

  Scenario: Redefinir senha de usuário ativo (uid:44bc686c-a627-42f3-9987-80dbbbfa5914)
    Given que sou um usuário ativo
    Then eu posso redefinir minha senha

  Scenario: Realizar login após redefinir senha (uid:620b6a3c-b488-43c2-a5b4-ea122d8c7ac1)
    Given que eu tenha redefinido a minha senha
    Then eu realizo o meu login com a minha senha antiga

  Scenario: Alterar dados de aluno com dados válidos (uid:dcfefeda-d84a-43e3-b970-5e7128e12543)
    Given que eu possua uma conta de aluno
    When eu altero meus dados de aluno com valores válidos
    Then salvo com sucesso as alterações

  Scenario: Alterar dados de aluno com dados inválidos (uid:a3fdc69c-f3bb-4298-9e50-9afd6ad48f9b)
    Given que eu possua uma conta de aluno
    When eu altero meus dados de aluno com valores inválidos
    Then não posso salvar meus dados

  Scenario: Alterar dados de aluno não preenchendo campos obrigatórios (uid:8412d332-aaeb-4359-bda2-55864cba60c6)
    Given que eu possua uma conta de aluno
    When eu altero meus dados não preenchendo os campos obrigatórios
    Then não posso salvar meus dados

@resp
  Scenario: Alterar dados de responsável com dados válidos (uid:9870bd8d-76a1-4896-8459-d832fd38d956)
    Given que eu possua uma conta de responsável
    When eu altero meus dados de responsável com valores válidos
    Then salvo com sucesso as alterações
@resp
  Scenario: Alterar dados de responsável com dados inválidos (uid:372e47fa-d664-446b-9b80-446165535fec)
    Given que eu possua uma conta de responsável
    When eu altero meus dados de responsável com valores inválidos
    Then não posso salvar meus dados
@resp
  Scenario: Alterar dados de responsável não preenchendo campos obrigatórios (uid:6eed6a3a-29f8-48d8-90ab-964b02a22b0d)
    Given que eu possua uma conta de responsável
    When eu altero meus dados não preenchendo os campos obrigatórios
    Then não posso salvar meus dados

  Scenario: Alterar dados de professor com dados válidos (uid:cff45ae6-da7c-4504-97e5-80406a655ec8)
    Given que eu possua uma conta de professor
    When eu altero meus dados de professor com valores válidos
    Then salvo com sucesso as alterações

  Scenario: Alterar dados de professor com dados inválidos (uid:d29818fd-48e5-4739-b6ae-3dd2de99d370)
    Given que eu possua uma conta de professor
    When eu altero meus dados de professor com valores inválidos
    Then não posso salvar meus dados

  Scenario: Alterar dados de professor não preenchendo campos obrigatórios (uid:bb97540e-63f9-46e3-bf41-7dd096daf9e7)
    Given que eu possua uma conta de professor
    When eu altero meus dados de professor não preenchendo os campos obrigatórios
    Then não posso salvar meus dados

  @altera_dados_escolares
  Scenario: Alterar dados escolares com dados válidos (uid:b7350ca7-e90e-4c4c-98b2-66d5c154cccb)
    Given que eu possua uma conta de professor
    When eu altero meus dados escolares com valores válidos
    Then salvo com sucesso as alterações dos dados escolares

  Scenario: Alterar dados escolares não preenchendo os campos obrigatórios (uid:9318ee57-9a82-4056-9b66-407d8e50ab3b)
    Given que eu possua uma conta de professor
    When eu não preencho os dados escolares obrigatórios
    Then não posso salvar meus dados

@user
Scenario Outline: Ativar um novo voucher de livro em conta criada com voucher de livro e turma (uid:6874031b-cfa2-4ea0-a013-0e7834bef39d)
    Given que eu realizo o login com meu "<login>"
    Then eu adiciono um novo voucher de livro na minha conta

Examples:
         | login                                 |                                 
         | ana.gonzaga+alunoteste@edumobi.com.br |
         | matheus.rodrigueslemos@hotmail.com    | 

@migracao_turmas
Feature: Migração de alunos para uma nova turma
        - Eu como administrador quero migrar todos os alunos para outra classe.
        - Essa feature é baseada G3-147
        
Background:
   Given que acesso a aplicação do gestor

@migracao_nova_turma_sem_manter_classe_antiga
Scenario: Migro alunos para uma nova turma sem manter a turma antiga
    When realizo a migração de alunos para uma nova turma
    Then escolho a nova turma com ano letivo "2019", segmento "Ensino Médio" e série "2ª Série" 
    
@migracao_turma_existente_mantendo_antiga
Scenario: Migração de alunos para uma nova turma mantendo a turma anterior
    When migro uma turma para outra turma existente
    Then escolho a nova turma com ano "2019", segmento "Ensino Médio", série "3ª Série" e seleciono a turma para migração

@migracao_turma_sem_material
Scenario: Migração de alunos para turma sem material
    When migro para a nova turma sem material com ano "2019", segmento "Pré-Vestibular" e série "Turmas de Maio"
    Then visualizo a mensagem "Não existem materiais disponíveis"

@migracao_classe_ja_existente
Scenario: Migração nova Turma com nome já existente
    When realizo a migração de alunos para uma nova turma
    Then escolho um nome já existente com ano "2019" segmento "Ensino Médio", série "1ª Série" e seleciono todos os materiais
    Then visualizo a mensagem "A turma informada já existe."

@migracao_de_turmas_sem_preencher_nome
Scenario: Migração de alunos para nova turma - com o campo nome da turma vazio
    When realizo a migração de alunos para uma nova turma
    When migro uma nova turma sem preeencher nome com ano "2019", segmento "Ensino Médio" e série "1ª Série" e seleciono todos os materiais
    Then o botão nova turma deverá estar desabilitado 

@migracao_turma_existente
Scenario: Migração de alunos para turmas existente sem manter a turma anterior
    When migro uma turma para outra turma existente
    Then escolho a nova turma com ano "2019", segmento "Ensino Médio", série "3ª Série" e seleciono a turma para migração

@migracao_nova_turma_mantendo_antiga
Scenario: Migração de alunos para uma nova turma porém mantendo a antiga
    When migro uma turma para outra turma nova
    Then escolho a nova turma com ano "2019", segmento "Ensino Médio", série "2ª Série" e seleciono todos os materiais
@inserir_matricula
Feature: Validação do campo Matrícula
        - Como coordenador ou equipe escolar .
        - Eu gostaria inserir dados no campo matrícula vazio ou editá-lo    
        
Background:
  Given que acesso a aplicação do gestor

@valida_matricula
Scenario: Insere matrícula via planilha com campo matrícula vazio 

When que crio uma turma via planilha com matricula campo vazio
Then acesso a opção de gerencia embarque do aluno
Then o link incluir matricula no campo matrícula está disponível

@matricula_preenchida
Scenario Outline: Insere matrícula via planilha campo preenchido

When que crio uma turma via planilha com matricula valida
Then acesso a opção de gerencia embarque do aluno
Then valido se o número da "<matricula>" é o mesmo da planilha

Examples:
        |matricula    |
        |MAT1233459009|

@matricula_planilha_repetida
Scenario: Insere matricula repetida via planilha 

When faço upload de planilha com matricula repetida 
Then valido a mensagem no popup informando dados incorretos

@altera_matricula
Scenario Outline: Altera matrícula ja existente

When acesso a opção de gerencia embarque do aluno
Then altero o "<valor_matricula>" da matricula
Then valido a "<mensagem_alteracao>" após alteração
 
Examples:
        |valor_matricula                                     | mensagem_alteracao                                                                     |
        | MAT1233459009                                      | A edição não pode ser realizada, pois o número de matrícula informado já foi utilizado.|  
        | 8wuwu-.                                            | Matrícula alterada com sucesso.                                                        |
        | 1239387fnefyg23yguguf-qwequweppPodayugguy111123333 | Matrícula alterada com sucesso.                                                        |
        | 123456                                             | Matrícula alterada com sucesso.                                                        |  

@altera_com_dados_errados
Scenario Outline: Altera matrícula existente colocando caracteres especiais

Then acesso a opção de gerencia embarque do aluno
Then altero o "<valor_matricula>" da matricula com dados incorretos
Then valido a "<mensagem_alteracao>" após alteração
And sistema não deve permitir salvar alteração da matricula
 
Examples:
        | valor_matricula | mensagem_alteracao                                    |
        | @#$_*&*%        | Há caracteres inválidos na matrícula. Favor revisá-la.|

@altera_ja_cadastrada
Scenario Outline: Altera matrícula ja cadastrada

Then acesso a opção de gerencia embarque do aluno
Then clico no link alterar matricula
Then altero o "<valor_matricula>" por um já cadastrado

Examples:
        |valor_matricula | 
        | MAT0000000666  | 

@envia_email_ativação_matricula
Scenario: Verificação matrícula após confirmação do envio de convites

Then acesso a opção de gerencia embarque do aluno
Then seleciono o voucher para envio de convites

@ativacao_matricula
Scenario Outline: Ativação da matrícula
Then encerro a sessão do somosid
Then acesso o "<email>" via mailinator para ativação da conta na plataforma
#Then valido "<matricula>" está ativa sem o link para alteração

Examples:
        |email                     | 
        |john_lenon@mailinator.com | 

@acesso_nominal
Scenario Outline: Criação de aluno nominal
Then acesso a opção de gerencia embarque do aluno
Then crio alunos via sistema na código de acesso nominal com dados "<nome_aluno>", "<valor_matricula>" e "<mensagem_alteracao>" do nome do aluno

Examples:
        |nome_aluno                |valor_matricula                                     | mensagem_alteracao                       |
        | Thiago da Silva          | MAT13121988                                        | Os alunos foram criados com sucesso!     |  
        | Ana da Silva             | 8wuwu-.                                            | Os alunos foram criados com sucesso!     |
        | Pedro da Silva           | 1239387fnefyg23yguguf-qwequweppPodayugguy111123333 | Os alunos foram criados com sucesso!     |
        | Alan da Silva            | 123456wsed                                         | Os alunos foram criados com sucesso!     |  
        
@acesso_nominal_dados_invalidos
Scenario Outline: Criação de aluno nominal dados inválidos
Then acesso a opção de gerencia embarque do aluno
Then digito caracteres especiais na "<valor_matricula>" via sistema na código de acesso nominal e valido a mensagem "<mensagem_alteracao>" 
 
Examples:
        | valor_matricula | mensagem_alteracao                                    |
        | @#$_*&*%        | Há caracteres inválidos na matrícula. Favor revisá-la.|
         
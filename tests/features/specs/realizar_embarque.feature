@somosidinterfaces @somosidglobal @somosidautomatizado @somosidautomatizado 
Feature: Realizar embarque
    # Como aluno, responsável ou equipe escolar eu posso me cadastrar utilizando um voucher e meu e-mail ou rede social.

  @criacao_responsavel_menor_setimo_ano
  Scenario: Criar cadastro de responsável de um aluno menor que sétimo ano com e-mail (uid:d030422e-1849-4c63-a3ef-2e93019ab9c3)
    Given que eu possuo um voucher de responsável de um aluno de EM - terceiro ano
    Then eu realizo o meu cadastro utilizando o meu e-mail

  @criacao_responsavel_maior_setimo_ano
  Scenario: Criar cadastro de responsável de um aluno maior que sétimo ano com e-mail (uid:42d9fb92-dbc3-4d8e-b2e8-6f244c88c11c)
    Given que eu possuo um voucher de responsável de um aluno de EF - sexto ano
    When eu realizo o meu cadastro utilizando o meu e-mail
    And cadastro meu filho

  Scenario: Criar cadastro de responsável de um aluno maior que sétimo ano com facebook (uid:c778c977-3a7a-403e-b727-8a1de856827f)
    Given que eu possua um voucher de responsável de EM Terceiro Ano e uma conta do facebook
    Then eu realizo o meu cadastro de responsável EM Terceiro Ano utilizando o facebook

  Scenario: Criar cadastro de responsável de um aluno menor que sétimo ano com o facebook (uid:b0b19124-209d-454e-a4c3-f035986166a8)
    Given que eu possua um voucher de responsável de EF Sexto Ano e uma conta do facebook
    When eu realizo o meu cadastro de responsável EF Sexto Ano utilizando o facebook
    And cadastro meu filho

  Scenario: Criar cadastro de responsável de um aluno maior que sétimo ano com google (uid:3158929d-ab68-4560-9cf8-88c33ef4148b)
    Given que eu possua um voucher de responsável de EM Terceiro Ano e uma conta do google
    Then eu realizo o meu cadastro de responsável EM Terceiro Ano utilizando o google

  Scenario: Criar cadastro de aluno utilizando voucher de livro (uid:ebfd5955-9181-4b7e-80d8-78dcfaca864f)
    Given que eu possuo um voucher de livro
    Then eu realizo o meu cadastro de aluno utilizando o meu e-mail

  @cadastro_voucher_turma
  Scenario: Criar cadastro de aluno utilizando voucher de turma (uid:bb861319-4e83-4d8e-9719-c3e707373fea)
    Given que eu possuo um voucher de aluno
    When eu realizo o meu cadastro de aluno utilizando o meu e-mail
    And tento reutilizar o meu voucher

  Scenario: Criar cadastro de aluno utilizando voucher de livro pelo facebook (uid:3869e217-1558-4fe3-a487-6f9d4ccc95a6)
    Given que eu possuo um voucher de livro e uma conta no facebook
    Then eu realizo o meu cadastro de aluno utilizando o facebook

  Scenario: Criar cadastro de um aluno maior que sétimo ano pelo facebook (uid:7d258024-def3-437f-a3ac-1b161f6b6558)
    Given que eu possua um voucher de aluno de EM Terceiro Ano e uma conta do facebook
    Then eu realizo o meu cadastro de aluno EM Terceiro Ano utilizando o facebook

  Scenario: Criar cadastro de aluno utilizando voucher de livro pelo google (uid:a7a50f99-dd3f-4714-bf9c-b5bac91d26bb)
    Given que eu possuo um voucher de livro e uma conta no google
    Then eu realizo o meu cadastro de aluno utilizando o google

  Scenario: Criar cadastro de um aluno maior que sétimo ano pelo google (uid:d103b11e-25b1-41e8-ac30-fc1ba24262e3)
    Given que eu possua um voucher de aluno de EM Terceiro Ano e uma conta do google
    Then eu realizo o meu cadastro de aluno EM Terceiro Ano utilizando o google

  Scenario: Criar cadastro de professor utilizando voucher de escola (uid:1c8cb395-85c0-4c4c-aa0f-0ef400e5826d)
    Given que eu possua um voucher de escola
    Then eu realizo o meu cadastro de professor utilizando o meu e-mail

  Scenario: Criar cadastro de professor utilizando voucher de livro (uid:a2d28956-e54a-4abc-9af8-7b90adbc1527)
    Given que eu possua um voucher de livro de professor
    Then eu realizo o meu cadastro de professor utilizando o meu e-mail e o voucher de escola

  Scenario: Criar cadastro de professor utilizando voucher de escola pelo facebook (uid:d2886b6e-1df0-4734-a053-e18340d2fe60)
    Given que eu possua uma conta no facebook
    Then eu realizo o meu cadastro de professor utilizando o voucher de escola

Feature: Adição de disciplinas extras o perfil do professor
        - Eu como professor quero alterar as disciplinas e turmas 
        - Para verificar se estarão disponíveis no gerenciar turma do gestor
        - Task baseada -> https://somoseducacao.atlassian.net/browse/SI19-42

Scenario: Altera disciplina
    Given que altero as disciplinas do professor na plataforma do gestor
    When aprovo alteração de cadastro com login de administrador
    Then valido se o perfil do professor está com as novas disciplinas

Scenario: Altera turma
    Given que altero as turmas do professor na plataforma do gestor
    When aprovo alteração de cadastro com login de administrador
    Then valido se o perfil do professor está com as novas turmas

Scenario: Exclui disciplina via gerenciar professor
    Given que deleto uma disciplina via gerenciar professor
    Then valido que não ficará mais disponível na tela

Scenario: Alterar material da turma
    Given que altero o material da turma
    Then as disciplinas do professor devem ser mantidas

    
        



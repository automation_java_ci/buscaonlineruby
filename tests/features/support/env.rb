# Importa as gems
require 'capybara'
require 'capybara/dsl'
require 'capybara/rspec/matchers'
require 'selenium-webdriver'
require 'rspec'
require 'faker'
require 'ostruct'
require 'site_prism'
require 'httparty'
require 'httparty/request'
require 'httparty/response/headers'
require 'byebug'
require 'cpf_faker'
require_relative 'page_helper.rb'
require_relative 'helper.rb'

# Coloca as variaveis como global
World(Capybara::DSL)
World(Capybara::RSpecMatchers)
World(Pages)
World(Helper)

def environment_type

  if ENV['ENVIRONMENT_TYPE'].nil? || ENV['ENVIRONMENT_TYPE'].empty?
    return 'qa'
  end

  ENV['ENVIRONMENT_TYPE']
end

def headless_enabled?
  #use headless as default mode
  ENV['HEADLESS'].nil? || ENV['HEADLESS'].empty? || ENV['HEADLESS'].eql?('com_headless')
end

def browser_driver_name
  if ENV['BROWSER_DRIVER'].nil? || ENV['BROWSER_DRIVER'].empty?
    return 'chrome'
  end

  ENV['BROWSER_DRIVER']
end

def register_chrome_browser(app)
  options = ::Selenium::WebDriver::Chrome::Options.new
  options.add_argument('--no-sandbox')
  options.add_argument('--disable-dev-shm-usage')
  options.add_argument('--window-size=1600,1024')


  if headless_enabled?
    options.add_argument('--headless')
    Capybara.javascript_driver = :chrome_headless
  end

  Capybara::Selenium::Driver.new(app,browser: :chrome,options: options)
end

def register_firefox_browser(app)
  driver_options = {
    browser: :firefox,
    marionette: true
  }

  if headless_enabled?
    driver_options[:options] = Selenium::WebDriver::Firefox::Options.new(args: ['--headless', '--disable-gpu', '--no-sandbox', '--window-size=1364,700'])
    driver_options[:marionette] = false
  end

  Capybara::Selenium::Driver.new(app, driver_options)
end

CONFIG = YAML.load_file(File.dirname(__FILE__) + "/data/#{environment_type}.yml")

# Configura o tipo de browser
Capybara.register_driver :selenium do |app|
  send("register_#{browser_driver_name}_browser", app)
end

Capybara.configure do |config|
  config.default_driver = :selenium
  config.default_max_wait_time = 50
  config.app_host = CONFIG['url_home']
end


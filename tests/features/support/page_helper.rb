Dir[File.join(File.dirname(__FILE__),
              '../pages/*_page.rb')].each { |file| require file }

# Modulos para chamar as classes instanciadas
module Pages

  def alteracao_email_professor
    @alteracao_email_professor ||= AlteracaoEmailProfessor.new
  end

  def migracao_turmas
    @migracao_turmas ||= MigracaoTurmas.new
  end

  def loginPage
    @loginPage ||= LoginPage.new
  end
  
  def embarque
    @embarque ||= Embarque.new
  end

  def usuarios_ativos
    @usuarios_ativos ||= UsuariosAtivos.new
  end

  def inserir_matricula
    @inserir_matricula ||= InserirMatricula.new
  end
end

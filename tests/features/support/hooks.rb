require_relative 'helper.rb'
require_relative 'env.rb'

World(Helper)

# metodo para alterar o nome da screnshot
# e direcionar se o teste falhou ou nao
After do |scenario|
  scenario_name = scenario.name.gsub(/\s+/, '_').tr('/', '_')
  scenario_name = scenario_name.gsub(',', '')
  scenario_name = scenario_name.gsub('(', '')
  scenario_name = scenario_name.gsub(')', '')
  scenario_name = scenario_name.gsub('#', '')

  if scenario.failed?
    take_screenshot(scenario_name.downcase!, 'failed')
  else
    take_screenshot(scenario_name.downcase!, 'passed')
  end
end

Before do |scenario|
  Capybara.current_session.driver.quit
end

# reseta os vouchers ao final do testes para serem utilizados novamente

#encoding:utf-8

When("realizo a migração de alunos para uma nova turma") do
  migracao_turmas.clica_migracao_aluno
  migracao_turmas.escolhe_nova_turma_exclui_antiga
end

Then("escolho a nova turma com ano letivo {string}, segmento {string} e série {string}") do |ano_letivo, segmento, serie|  
  migracao_turmas.escolhe_segmento_serie(ano_letivo, segmento, serie)
end

When("migro uma turma para outra turma nova") do
  migracao_turmas.migracao_turmas_via_nova_funcionalidade
  migracao_turmas.radio_nova_turma.click
end

Then("escolho a nova turma com ano {string}, segmento {string}, série {string} e seleciono todos os materiais") do |ano_letivo, segmento, serie|
  migracao_turmas.escolhe_segmento_serie(ano_letivo, segmento, serie)  
  expect(page).to have_content("Os alunos foram migrados com sucesso!")
end

When("migro uma turma para outra turma existente") do
  migracao_turmas.migracao_turmas_via_nova_turmas_existente
end

Then("escolho a nova turma com ano {string}, segmento {string}, série {string} e seleciono a turma para migração") do |ano_letivo, segmento, serie|
  migracao_turmas.escolhe_migracao_turma_existente(ano_letivo, segmento, serie)
  expect(page).to have_content("Os alunos foram migrados com sucesso!")
end

When("migro para a nova turma sem material com ano {string}, segmento {string} e série {string}") do |ano_letivo, segmento, serie|
  migracao_turmas.migracao_turmas_via_nova_funcionalidade
  migracao_turmas.escolhe_segmento_serie_sem_material(ano_letivo, segmento, serie)   
end

Then("visualizo a mensagem {string}") do |mensagem|
  expect(page).to have_content(mensagem)
end

When("escolho um nome já existente com ano {string} segmento {string}, série {string} e seleciono todos os materiais") do |ano_letivo, segmento, serie|
  migracao_turmas.escolhe_nome_repetido
  migracao_turmas.escolhe_segmento_serie(ano_letivo, segmento, serie)
end

When("migro uma nova turma sem preeencher nome com ano {string}, segmento {string} e série {string} e seleciono todos os materiais") do |ano_letivo, segmento, serie|
  migracao_turmas.escolhe_segmento_serie_sem_material(ano_letivo, segmento, serie)
end

Then("o botão nova turma deverá estar desabilitado") do
  migracao_turmas.btn_criar_turma.disabled?
end
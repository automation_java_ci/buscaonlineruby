Given("que acesso a aplicação do gestor") do
    steps %Q{
        Given que acesso escola via gestor     
    }
end

Then("que crio uma turma via planilha com matricula campo vazio") do
    inserir_matricula.criar_turma_matricula_vazia
    inserir_matricula.valida_turma_criada
end

When("acesso a opção de gerencia embarque do aluno") do
    inserir_matricula.clica_gerenciar_embarque
end

Then("o link incluir matricula no campo matrícula está disponível") do
    inserir_matricula.valida_link_incluir_matricula
end

Then("que crio uma turma via planilha com matricula valida") do
    inserir_matricula.criar_turma_matricula_preenchida
    inserir_matricula.valida_turma_criada
end

Then("valido se o número da {string} é o mesmo da planilha") do |matricula|
    inserir_matricula.valida_matricula(matricula)
    inserir_matricula.valida_link_alterar_matricula
end

Then("altero o {string} da matricula") do |valor_matricula|
   inserir_matricula.clico_alterar_matricula
   inserir_matricula.digita_nova_matricula(valor_matricula)
   inserir_matricula.botao_salvar_alteracao.click
end

Then("valido a {string} após alteração") do |mensagem_alteracao|
    inserir_matricula.valida_mensagem_alteracao_sucesso(mensagem_alteracao)
end
  

Then("faço upload de planilha com matricula repetida") do
    inserir_matricula.criar_turma_matricula_preenchida
end

Then("valido a mensagem no popup informando dados incorretos") do
    inserir_matricula.valida_mensagem_matricula_repetida
end

When("clico no link alterar matricula") do
    inserir_matricula.clico_alterar_matricula
end

Then("altero o {string} por um já cadastrado") do |valor_matricula|
    inserir_matricula.digita_nova_matricula(valor_matricula)
    inserir_matricula.botao_salvar_alteracao.click
    inserir_matricula.valida_mensagem_matricula_repetida
end


When("altero o {string} para um dado incorreto") do |valor_matricula|
    inserir_matricula.digita_nova_matricula(valor_matricula)
end

Then("valido a {string} de erro para caracteres especiais") do |mensagem_alteracao|
    inserir_matricula.valida_mensagem_alteracao_caracteres_invalidos(mensagem_alteracao)    
end

Then("altero o {string} da matricula com dados incorretos") do |valor_matricula|
    inserir_matricula.clico_alterar_matricula
    inserir_matricula.digita_nova_matricula(valor_matricula)
end

Then("sistema não deve permitir salvar alteração da matricula") do
   inserir_matricula.botao_salvar_alteracao.disabled?
end

Then("seleciono o voucher para envio de convites") do
    inserir_matricula.click_box_convites
    inserir_matricula.enviar_convites
end

Then("encerro a sessão do somosid") do
    sleep(5)
    visit "/logout"
end

Then("acesso o {string} via mailinator para ativação da conta na plataforma") do |email|
    inserir_matricula.acesso_mailinator(email)
end

Then("crio alunos via sistema na código de acesso nominal com dados {string}, {string} e {string} do nome do aluno") do |nome_aluno, valor_matricula, mensagem_alteracao|
    inserir_matricula.clicar_criacao_aluno_via_sistema_nominal(nome_aluno, valor_matricula, mensagem_alteracao)
end

Then("digito caracteres especiais na {string} via sistema na código de acesso nominal e valido a mensagem {string}") do |valor_matricula, mensagem_alteracao|
    inserir_matricula.clicar_criacao_aluno_via_sistema_caracteres_especiais(valor_matricula, mensagem_alteracao)
end
Given("que acesso a aba dados pessoais do somosid") do
  alteracao_email_professor.acessa_dados_pessoais
end

When("eu altero meu {string} para um email correto") do |email_professor|
  alteracao_email_professor.alteracao_email(email_professor)
end

Then("eu altero meu {string} para um email inválido") do |email_professor|
  alteracao_email_professor.alteracao_email_incorreto(email_professor)
end

Then("valido {string} de sucesso") do |mensagem|
   expect(page).to have_content(mensagem)
end

Then("ativo o novo {string} e realizo novo login") do |email_professor|
  alteracao_email_professor.ativacao_novo_email(email_professor)
  alteracao_email_professor.realiza_novo_login
end

Then("altero as informações de cadastro novamente") do
  
end

Then("eu altero meu {string}") do |email_professor|
  alteracao_email_professor.alteracao_email(email_professor)
end

Then("valido {string} de alteração inválida") do |mensagem|
  expect(page).to have_content(mensagem)  
end

Then("valido botão salvar desabilitado") do
  alteracao_email_professor.btn_salvar.disabled?
end
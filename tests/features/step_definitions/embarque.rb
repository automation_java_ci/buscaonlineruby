
Dado("que eu possuo um voucher de responsável de um aluno de EM - terceiro ano") do
  visit('/')
  embarque.novo_cadastro.click
  embarque.codigo_acesso.set('ZXXNNDCSJ')
  embarque.prosseguir.click
end

Quando("eu realizo o meu cadastro utilizando o meu e-mail") do
  embarque.confirmar_dados.click
  name_faker = Faker::Name.first_name + Faker::Number.number(5)
  mailinator = "#{name_faker}@mailinator.com"
  embarque.nome.set(name_faker)
  embarque.email.set(mailinator)
  embarque.email.click
  embarque.confirma_email.set(mailinator)
  embarque.confirma_email.click
  embarque.aceita_termos.click
  embarque.prosseguir.click
  embarque.senha.set(CONFIG['senha'])
  embarque.confirma_senha.set(CONFIG['senha'])
  embarque.prosseguir.click
  has_content? mailinator
end

Dado("que eu possuo um voucher de responsável de um aluno de EF - sexto ano") do
  visit('/')
  embarque.novo_cadastro.click
  embarque.codigo_acesso.set('ZXXH62GH7')
  embarque.prosseguir.click
end

Então("cadastro meu filho") do
  name_faker = Faker::Name.first_name + Faker::Number.number(5)
  mailinator = name_faker
  embarque.nome_aluno.set(name_faker)
  embarque.email_aluno.set(mailinator)
  embarque.senha_aluno.set(CONFIG['senha'])
  embarque.confirma_senha_aluno.set(CONFIG['senha'])
  embarque.prosseguir.click
 end

 Dado("que eu possua um voucher de responsável de EM Terceiro Ano e uma conta do facebook") do
  visit('/')
  @user_fb = OpenStruct.new
  @user_fb.login_fb = 'responsavel_xlnupfj_dois@tfbnw.net'
  @user_fb.senha_fb = 'somosid123'
  embarque.logar_facebook(@user_fb)
  embarque.codigo_acesso.set('ZXXNWB2B7')
  embarque.prosseguir.click
end

Então("eu realizo o meu cadastro de responsável EM Terceiro Ano utilizando o facebook") do
  embarque.confirmar_dados.click
  embarque.aceita_termos.click
  embarque.prosseguir.click
end

Dado("que eu possua um voucher de responsável de EF Sexto Ano e uma conta do facebook") do
  visit('/')
  @user_fb = OpenStruct.new
  @user_fb.login_fb = 'responsavel_xlnupfj_dois@tfbnw.net'
  @user_fb.senha_fb = 'somosid123'
  embarque.logar_facebook(@user_fb)
  embarque.codigo_acesso.set('ZXXHDETH7')
  embarque.prosseguir.click
end

Quando("eu realizo o meu cadastro de responsável EF Sexto Ano utilizando o facebook") do
  embarque.confirmar_dados.click
  embarque.aceita_termos.click
  embarque.prosseguir.click
  page.has_title? "Minha Conta"
end

Dado("que eu possua um voucher de responsável de EM Terceiro Ano e uma conta do google") do
  visit('/')
  @user_google = OpenStruct.new
  @user_google.login_google = 'somosidtcs@gmail.com'
  @user_google.senha_google = 'edumobi123456!'
  embarque.logar_google(@user_google)
  embarque.codigo_acesso.set('ZXXNR3JSX')
  embarque.prosseguir.click
end

Então("eu realizo o meu cadastro de responsável EM Terceiro Ano utilizando o google") do
  embarque.confirmar_dados.click
  embarque.aceita_termos.click
  embarque.prosseguir.click
  page.has_title? "Minha Conta"
end

Dado("que eu possuo um voucher de livro") do
  visit('/')
  embarque.novo_cadastro.click
  embarque.codigo_acesso.set('TESTEAUTOW4UQBKPR9DZ')
  embarque.prosseguir.click
end

Então("eu realizo o meu cadastro de aluno utilizando o meu e-mail") do
  name_faker = Faker::Name.first_name + Faker::Number.number(5)
  mailinator = "#{name_faker}@mailinator.com"
  embarque.nome.set(name_faker)
  embarque.email.set(mailinator)
  embarque.email.click
  embarque.confirma_email.set(mailinator)
  embarque.confirma_email.click
  embarque.aceita_termos.click
  embarque.prosseguir.click
  embarque.senha.set(CONFIG['senha'])
  embarque.confirma_senha.set(CONFIG['senha'])
  embarque.prosseguir.click
  has_content? mailinator
end

Dado("que eu possuo um voucher de aluno") do
  visit('/')
  embarque.novo_cadastro.click
  embarque.codigo_acesso.set('ZXXNMKXPF')
  embarque.prosseguir.click
  embarque.confirmar_dados.click
end

Então("tento reutilizar o meu voucher") do
  visit('/')
  embarque.novo_cadastro.click
  embarque.codigo_acesso.set('ZXXNMKXPF')
  embarque.prosseguir.click
  has_content? 'Este código de acesso excedeu o limite de uso.'
end

Dado("que eu possuo um voucher de livro e uma conta no facebook") do
  visit('/')
  @user_fb = OpenStruct.new
  @user_fb.login_fb = 'aluno_sghawqd_de_livros@tfbnw.net'
  @user_fb.senha_fb = 'somosid123'
  embarque.logar_facebook(@user_fb)
  embarque.codigo_acesso.set('TESTEAUTOW4UQBKPR9DZ')
  embarque.prosseguir.click
end

Então("eu realizo o meu cadastro de aluno utilizando o facebook") do
  embarque.aceita_termos.click
  embarque.prosseguir.click
  page.has_title? "Minha Conta"
end

Dado("que eu possua um voucher de aluno de EM Terceiro Ano e uma conta do facebook") do
  visit('/')
  @user_fb = OpenStruct.new
  @user_fb.login_fb = 'aluno_sghawqd_de_livros@tfbnw.net'
  @user_fb.senha_fb = 'somosid123'
  embarque.logar_facebook(@user_fb)
  embarque.codigo_acesso.set('ZXXNMZU4T')
  embarque.prosseguir.click
end

Então("eu realizo o meu cadastro de aluno EM Terceiro Ano utilizando o facebook") do
  embarque.confirmar_dados.click
  embarque.aceita_termos.click
  embarque.prosseguir.click
  page.has_title? "Minha Conta"
end

Dado("que eu possuo um voucher de livro e uma conta no google") do
  visit('/')
  @user_google = OpenStruct.new
  @user_google.login_google = 'somosidtcs@gmail.com'
  @user_google.senha_google = 'edumobi123456!'
  embarque.logar_google(@user_google)
  embarque.codigo_acesso.set('TESTEAUTOW4UQBKPR9DZ')
  embarque.prosseguir.click
end

Então("eu realizo o meu cadastro de aluno utilizando o google") do
  embarque.aceita_termos.click
  embarque.prosseguir.click
  page.has_title? "Minha Conta"
end

Dado("que eu possua um voucher de aluno de EM Terceiro Ano e uma conta do google") do
  visit('/')
  @user_google = OpenStruct.new
  @user_google.login_google = 'somosidtcs@gmail.com'
  @user_google.senha_google = 'edumobi123456!'
  embarque.logar_google(@user_google)
  embarque.codigo_acesso.set('ZXXN9HHNE')
  embarque.prosseguir.click
end

Então("eu realizo o meu cadastro de aluno EM Terceiro Ano utilizando o google") do
  embarque.confirmar_dados.click
  embarque.aceita_termos.click
  embarque.prosseguir.click
  page.has_title? "Minha Conta"
end

Dado("que eu possua um voucher de escola") do
  visit('/')
  embarque.novo_cadastro.click
  embarque.codigo_acesso.set('YXXXMU36W')
  embarque.prosseguir.click
end

Então("eu realizo o meu cadastro de professor utilizando o meu e-mail") do
  name_faker = Faker::Name.first_name + Faker::Number.number(5)
  mailinator = "#{name_faker}@mailinator.com"
  embarque.cpf_professor.set(Faker::CPF.numeric)
  embarque.nome.set(name_faker)
  embarque.email.set(mailinator)
  embarque.email.click
  embarque.confirma_email.set(mailinator)
  embarque.confirma_email.click
  embarque.aceita_termos.click
  embarque.prosseguir.click
  embarque.senha.set(CONFIG['senha'])
  embarque.confirma_senha.set(CONFIG['senha'])
  embarque.prosseguir.click
  embarque.selecionar_professor
  page.has_text? mailinator
end

Dado("que eu possua um voucher de livro de professor") do
  visit('/')
  embarque.novo_cadastro.click
  embarque.codigo_acesso.set('AASGC99Z9')
  embarque.prosseguir.click
end

Então("eu realizo o meu cadastro de professor utilizando o meu e-mail e o voucher de escola") do
  name_faker = Faker::Name.first_name + Faker::Number.number(5)
  mailinator = "#{name_faker}@mailinator.com"
  embarque.nome.set(name_faker)
  embarque.email.set(mailinator)
  embarque.email.click
  embarque.confirma_email.set(mailinator)
  embarque.confirma_email.click
  embarque.aceita_termos.click
  embarque.prosseguir.click
  embarque.senha.set(CONFIG['senha'])
  embarque.confirma_senha.set(CONFIG['senha'])
  embarque.prosseguir.click
  has_content? mailinator
end

Dado("que eu possua uma conta no facebook") do
  visit('/')
  @user_fb = OpenStruct.new
  @user_fb.login_fb = 'professor_telefsk_turma@tfbnw.net'
  @user_fb.senha_fb = 'somosid123'
  embarque.logar_facebook(@user_fb)
  embarque.codigo_acesso.set('YXXXMU36W')
  embarque.prosseguir.click
end

Então("eu realizo o meu cadastro de professor utilizando o voucher de escola") do
  embarque.cpf_professor.set('782.022.201-81')
  embarque.aceita_termos.click
  embarque.prosseguir.click
  embarque.selecionar_professor
  page.has_title? "Minha Conta"
end
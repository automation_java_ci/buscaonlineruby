Dado("que eu realizo o login com meu {string}") do |login|
  visit('/')
  usuarios_ativos.login.set(login)
  usuarios_ativos.senha.set(CONFIG['senha'])
  usuarios_ativos.entrar.click
end

Então("eu adiciono um novo voucher de livro na minha conta") do
  usuarios_ativos.voucher.set('TESTEAUTOW4UQBKPR9DZ')
  usuarios_ativos.wait_until_salvar_visible
  usuarios_ativos.salvar.click
  has_selector? '.alert-success'
end

Dado("que sou um usuário ativo") do
  visit('/')
  usuarios_ativos.esqueci_senha.click
end

Então("eu posso redefinir minha senha") do
  usuarios_ativos.email.set('ana.gonzaga+aluno.faraday@edumobi.com.br')
  usuarios_ativos.enviar.click
  page.has_text? 'Um email com um link para redefinição de senha foi enviado para ana.gonzaga+aluno.faraday@edumobi.com.br'
end

Dado("que eu tenha redefinido a minha senha") do
  visit('/')
  usuarios_ativos.login.set('ana.gonzaga+aluno.faraday@edumobi.com.br')
  usuarios_ativos.senha.set('123456')
  usuarios_ativos.entrar.click
end

Então("eu realizo o meu login com a minha senha antiga") do
  page.has_text? 'Minha Conta'
end

Dado("que eu possua uma conta de aluno") do
  visit('/')
  usuarios_ativos.login.set('ana.gonzaga+aluno.faraday@edumobi.com.br')
  usuarios_ativos.senha.set(CONFIG['senha'])
  usuarios_ativos.entrar.click
end

Então("eu altero meus dados de aluno com valores válidos") do
  usuarios_ativos.dados_pessoais.click
  @user = OpenStruct.new
  usuarios_ativos.nome.send_keys :backspace
  @user.nome = usuarios_ativos.nome.value + Faker::Number.number(1)
  @user.data_nascimento = rand(10..31).to_s + rand(10..12).to_s + rand(1997..2005).to_s
  usuarios_ativos.minha_conta(@user)
end

Então("eu altero meus dados de aluno com valores inválidos") do
  usuarios_ativos.dados_pessoais.click
  @user = OpenStruct.new
  usuarios_ativos.nome.native.clear
  @user.data_nascimento = rand(10..31).to_s + rand(10..12).to_s + rand(1..10).to_s
  usuarios_ativos.minha_conta(@user)
  expect(page).to have_button('Salvar', disabled: true)
end

Então("salvo com sucesso as alterações") do
  usuarios_ativos.salvar.click
  page.has_text? 'Usuário atualizado com sucesso!'
end

Então("não posso salvar meus dados") do
  expect(page).to have_button('Salvar', disabled: true)
end

Quando("eu altero meus dados não preenchendo os campos obrigatórios") do
  usuarios_ativos.dados_pessoais.click
  usuarios_ativos.nome.set(" ")
  usuarios_ativos.nome.send_keys :backspace
  usuarios_ativos.email.set(" ")
  page.assert_text('Este campo é obrigatório')
  usuarios_ativos.email.send_keys :backspace
  usuarios_ativos.data_nascimento.send_keys :backspace
  page.assert_text('Este campo é obrigatório')
  usuarios_ativos.nome.click
  page.assert_text('Este campo é obrigatório')
end

Dado("que eu possua uma conta de professor") do
  visit('/') 
  usuarios_ativos.login.set('marcoscimino2003@yahoo.com.br')
  usuarios_ativos.senha.set(CONFIG['senha'])
  usuarios_ativos.entrar.click
end

Quando("eu altero meus dados de professor com valores válidos") do
  usuarios_ativos.dados_pessoais.click
  usuarios_ativos.nome.send_keys :backspace
  usuarios_ativos.nome.set((usuarios_ativos.nome.value + rand(1..9).to_s))
  usuarios_ativos.cpf.set(Faker::CPF.numeric)
  usuarios_ativos.ddd.set("1 + Faker::Number.number(1)")
  usuarios_ativos.telefone.set(Faker::PhoneNumber.subscriber_number(9))
  usuarios_ativos.data_nascimento.set(rand(10..31).to_s + rand(10..12).to_s + rand(1997..2005).to_s)
end

Quando("eu altero meus dados de professor com valores inválidos") do
  usuarios_ativos.dados_pessoais.click
  usuarios_ativos.nome.send_keys :backspace
  usuarios_ativos.nome.set(usuarios_ativos.nome.value + rand(1..9).to_s)
  usuarios_ativos.cpf.set('123456')
  page.assert_text('CPF inválido')
  usuarios_ativos.email.set('marciocimino2033')
  usuarios_ativos.ddd.set("1 + Faker::Number.number(1)")
  usuarios_ativos.telefone.set(Faker::PhoneNumber.subscriber_number(9))
  usuarios_ativos.data_nascimento.set(rand(10..31).to_s + rand(10..12).to_s)
  usuarios_ativos.ddd.click
  page.assert_text('Digite uma data válida')
end

Quando("eu altero meus dados de professor não preenchendo os campos obrigatórios") do
  usuarios_ativos.dados_pessoais.click
  usuarios_ativos.nome.send_keys :backspace
  usuarios_ativos.nome.set((usuarios_ativos.nome.value + rand(1..9).to_s))
  usuarios_ativos.cpf.set(" ")
  usuarios_ativos.cpf.send_keys :backspace
  page.assert_text('Este campo é obrigatório')
  usuarios_ativos.ddd.set(" ")
  usuarios_ativos.ddd.send_keys :backspace
  page.assert_text('Este campo é obrigatório')
  usuarios_ativos.telefone.set(" ")
  usuarios_ativos.telefone.send_keys :backspace
  page.assert_text('Este campo é obrigatório')
  usuarios_ativos.data_nascimento.set(rand(10..31).to_s + rand(10..12).to_s)
  usuarios_ativos.ddd.click
  page.assert_text('Digite uma data válida')
end

Quando("eu altero meus dados escolares com valores válidos") do
  usuarios_ativos.dados_escolares.click
  usuarios_ativos.escolher_escola.click
  usuarios_ativos.tamandare.click
  usuarios_ativos.wait_until_educacao_infantil_visible
  usuarios_ativos.educacao_infantil.click
  usuarios_ativos.wait_until_selecionar_disciplinas_visible
  usuarios_ativos.selecionar_disciplinas.click
  usuarios_ativos.wait_until_material_basico_visible
  usuarios_ativos.material_basico.click
  usuarios_ativos.selecionar.click 
end

Então("salvo com sucesso as alterações dos dados escolares") do
  usuarios_ativos.salvar_dados.click
  expect(page).to have_content('Informações salvas com sucesso! Suas alterações foram enviadas para aprovação e você receberá um e-mail assim que forem aprovadas.')
  usuarios_ativos.educacao_infantil.click
  usuarios_ativos.wait_until_salvar_dados_visible
  usuarios_ativos.salvar_dados.click
end

Quando("eu não preencho os dados escolares obrigatórios") do
  usuarios_ativos.dados_escolares.click
  usuarios_ativos.escolher_escola.click
  usuarios_ativos.tamandare.click
  usuarios_ativos.professor.click
end

Dado("que eu possua uma conta de responsável") do
  visit('/') 
  usuarios_ativos.login.set('responsavel.teste1@anglobauru.com.br')
  usuarios_ativos.senha.set(CONFIG['senha'])
  usuarios_ativos.entrar.click
end

Quando("eu altero meus dados de responsável com valores válidos") do
  usuarios_ativos.dados_pessoais.click
  @user = OpenStruct.new
  usuarios_ativos.nome.send_keys :backspace
  @user.nome = usuarios_ativos.nome.value + Faker::Number.number(1)
  @user.data_nascimento = rand(10..31).to_s + rand(10..12).to_s + rand(1950..1980).to_s
  usuarios_ativos.minha_conta(@user)
end

Quando("eu altero meus dados de responsável com valores inválidos") do
  usuarios_ativos.dados_pessoais.click
  @user = OpenStruct.new
  usuarios_ativos.nome.native.clear
  @user.data_nascimento = rand(10..31).to_s + rand(10..12).to_s + rand(1..10).to_s
  usuarios_ativos.minha_conta(@user)
  expect(page).to have_button('Salvar', disabled: true)
end
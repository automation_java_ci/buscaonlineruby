class MigracaoTurmas < SitePrism::Page
   
element :btn_acoes, 'button[class="dropdown-button btn btn-select waves-effect full-width text-left no-margin"]'
element :link_excluir_turma, 'a[title="Excluir Turma"]'
element :campo_nome_turma, 'input[type="text"]'
element :radio_nova_turma_excluir_antiga, 'label[for="ExclusaoTurmaNova"]'
element :radio_nova_turma, 'label[for="MigrarTurmaNova"]'
element :radio_turma_ja_existente, 'label[for="MigrarTurmaExistente"]'
element :select_ano_letivo, 'select[ng-model="turma.periodo"]'
element :select_segmento, 'select[ng-model="turma.segmento"]'
element :select_serie, 'select[ng-model="turma.serie"]'
element :check_option, 'div[class="multiSelectAllItem"]'
element :btn_cancelar, 'button[class="modal-action modal-close btn btn-danger"]'
element :btn_criar_turma, 'button[class="btn waves-effect"]'
element :migracao_turma_nova_funcinalidade, 'a[title="Migrar Alunos - Funcionalidade nova!"]'
element :select_nova_turma, 'select[ng-model="filtroModal.turmaSelecionada"]'
element :select_ano_turma_existente, 'select[ng-model="filtroModal.periodo"]'
element :select_segmento_turma_existente, 'select[ng-model="filtroModal.segmentoSelecionado"]'
element :select_serie_turma_existente, 'select[ng-model="filtroModal.serieSelecionada"]'

def clica_migracao_aluno
    wait_until_btn_acoes_visible
    find(:css,'.dropdown-button.btn.btn-select.waves-effect.full-width.text-left.no-margin', match: :first).click
    link_excluir_turma.click
end

def migracao_turmas_via_nova_funcionalidade
   wait_until_btn_acoes_visible
   @time = Time.new 
   find(:css,'.dropdown-button.btn.btn-select.waves-effect.full-width.text-left.no-margin', match: :first).click 
   migracao_turma_nova_funcinalidade.click
   campo_nome_turma.set "Nova turma de Migração - Via nova funcionalidade #{@time.strftime("%Y-%m-%eT%H:%M")}" 
end


  def migracao_turmas_via_nova_turmas_existente
    wait_until_btn_acoes_visible
    @time = Time.new 
    find(:css,'.dropdown-button.btn.btn-select.waves-effect.full-width.text-left.no-margin', match: :first).click 
    migracao_turma_nova_funcinalidade.click
    radio_turma_ja_existente.click
  end

  def escolhe_nova_turma_exclui_antiga
    @time = Time.new
    radio_nova_turma_excluir_antiga.click
    campo_nome_turma.set "Nova turma de Migração #{@time.strftime("%Y-%m-%eT%H:%M")}"
  end

  def escolhe_nome_repetido
    campo_nome_turma.set "2.1 AM"
  end

  def escolhe_migracao_turma_existente(ano_letivo, segmento, serie)
    select_ano_turma_existente.find('option', text: ano_letivo).select_option 
    select_segmento_turma_existente.find('option', text: segmento).select_option
    select_serie_turma_existente.find('option', text: serie).select_option
    select_nova_turma.find('option', text: "2.3 AM").select_option
    click_button("Migrar alunos")
  end

  def escolhe_segmento_serie(ano_letivo, segmento, serie)
    select_ano_letivo.send_keys(:tab)
    select_ano_letivo.find('option', text: ano_letivo).select_option 
    select_segmento.find('option', text: segmento).select_option
    select_serie.find('option', text: serie).select_option
    click_button('Selecione o material')
    check_option.click
    click_button('[4] Todos selecionados')
    btn_criar_turma.click
  end

  
  def escolhe_segmento_serie_sem_material(ano_letivo, segmento, serie)
    wait_until_select_ano_letivo_visible
    select_ano_letivo.send_keys(:tab)
    select_ano_letivo.find('option', text: ano_letivo).select_option 
    select_segmento.find('option', text: segmento).select_option
    select_serie.find('option', text: serie).select_option
   
  end

  def clica_migracao_nova_turma
    wait_until_btn_acoes_visible
    find(:css,'.dropdown-button.btn.btn-select.waves-effect.full-width.text-left.no-margin', match: :first).click
    link_migrar_aluno.click
    sleep(10)
  end


end
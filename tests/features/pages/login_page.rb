class LoginPage < SitePrism::Page
    element :login, '#username'
    element :senha, '#senha'
    element :entrar, 'input[value="Entrar"]'
   
    def acesso_gestor
        find('span', text: "Gestor de Turmas").click
        page.driver.browser.switch_to().window(page.driver.browser.window_handles.last)
    end

    def seleciona_escola
        find('option[value="4214313"]').click
        click_button('Continuar') 
    end
end
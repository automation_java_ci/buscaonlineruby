class InserirMatricula < SitePrism::Page
    element :campo_nome, 'input[name="nome"]'
    element :combo_segmento, 'select[name="segmento"]'
    element :combo_serie, 'select[name="serie"]'
    element :check_option, 'div[class="multiSelectAllItem"]'
    element :seleciona_material, 'button[type="button"]'
    element :confere_turma_criada, 'label[class="ng-scope"]'                                          
    element :acoes, 'button[class="dropdown-button btn btn-select waves-effect full-width text-left no-margin"]'
    element :link_matricula, 'a[class="modal-trigger"]'
    element :icone_incluir_matricula, 'i[class=fa fa-plus-circle]'
    element :campo_matricula, '#matricula'
    element :botao_salvar_alteracao, 'button[class="btn"]'
    element :mensagem_alteracao_sucesso, '.div.alert.alert-success.ng-binding'
    element :check_convites, 'label[for="1"]' 
    element :campo_email_mailinator, '#inboxfield'
    element :mensagem_envio_voucher, 'div[class="alert alert-success ng-binging ng-scope"]'
    element :confirma_email, 'input[name="confirmaEmail"]'
    element :check_confirmacao_termo, 'div.box-perfil.terms > label > span' 
    element :passwd_cadastro, '#password'
    element :botao_prosseguir, 'input[type="submit"]'
    element :passwd_cadastro_confirmacao, '#password-confirm'
    element :botao_criar_novo_alunos, 'button[class="dropdown-button btn btn-select waves-effect active"]'
    element :voucher_nominal, 'label[for="voucherNominal"]'
    element :campo_nome_aluno, 'input[placeholder="Insira aqui o nome do aluno"]'
    element :botao_criar_alunos, 'button[class="btn waves-effect"]'   


   def click_box_convites
      if page.has_text?('Alterar matrícula')
         check_convites.click
      else 
         page.evaluate_script("window.location.reload()")   
         check_convites.click
      end
   end

   def enviar_convites
      click_button('Enviar convites')
      click_button('Enviar convite(s)')
      page.assert_text("convite(s) enviado(s) com sucesso.")
   end

   def acesso_mailinator(email)
      visit "https://www.mailinator.com/"
      campo_email_mailinator.set email
      click_button('Go!')
      find(:xpath, "//td[@class='ng-binding' and contains(text(),'no-reply@somosid.com.br')]", match: :first).click
      
      within_frame("msg_body") do
         find(:link, 'Clique para acessar').click

         
    end    
   
         page.driver.browser.switch_to().window(page.driver.browser.window_handles.last)
         
         page.evaluate_script("window.location.reload()")


         click_button("Confirmar dados! :)")
         wait_until_confirma_email_visible
         confirma_email.set email
         check_confirmacao_termo.click
         botao_prosseguir.click
         
         wait_until_passwd_cadastro_visible
         passwd_cadastro.set "somosteste123456"
         passwd_cadastro_confirmacao.set "somosteste123456"
         botao_prosseguir.click 
         
         
              
          
   end


   def criar_turma_matricula_vazia
      click_button('Criar uma turma')      
      click_button('Continuar')
      @time = Time.new
      campo_nome.set "Turma com campo matrícula vazio #{ @time.strftime("%Y-%m-%eT%H:%M")}"
      combo_segmento.find(:xpath, 'option[5]').select_option
      combo_serie.find(:xpath, 'option[4]').select_option
      combo_serie.send_keys(:tab)
      click_button('Selecione o material')
      check_option.click
      @arquivo_planilha = File.join(Dir.pwd, 'resources/Turma_campo_matricula_vazio.xls')
      attach_file('new-class-file', @arquivo_planilha, make_visible: true)        
      click_button('Criar Turma')
   end
   
   def criar_turma_matricula_preenchida
      click_button('Criar uma turma')      
      click_button('Continuar')
      @time = Time.new
      campo_nome.set "Turma com campo matrícula preenchida #{ @time.strftime("%Y-%m-%eT%H:%M")}"
      combo_segmento.find(:xpath, 'option[5]').select_option
      combo_serie.find(:xpath, 'option[4]').select_option
      combo_serie.send_keys(:tab)
      click_button('Selecione o material')
      check_option.click
      @arquivo_planilha = File.join(Dir.pwd, 'resources/Turma_campo_matricula_preenchido.xls')
      attach_file('new-class-file', @arquivo_planilha, make_visible: true)        
      click_button('Criar Turma')
   end

   def valida_matricula(matricula)
      page.assert_text(matricula)
   end

   def valida_link_alterar_matricula
      page.assert_text('(Alterar matrícula)')
   end

   def valida_turma_criada
      wait_until_confere_turma_criada_visible    
      click_button('Fechar')
   end

   def clica_gerenciar_embarque
      find(:css,'.dropdown-button.btn.btn-select.waves-effect.full-width.text-left.no-margin', match: :first).click
      find(:link,'Gerenciar embarque de alunos').click   
   end

   def valida_link_incluir_matricula      
      wait_until_link_matricula_visible
      find_link("Incluir matrícula").visible?
   end

   def valida_mensagem_matricula_repetida
      page.assert_text('O número de matrícula informado já foi utilizado')
   end
   
   def clico_alterar_matricula
      find(:link, "(Alterar matrícula)", match: :first).click
   end

   def digita_nova_matricula(valor_matricula)
         page.assert_text('Alteração de matrícula do Aluno') 
         campo_matricula.set ""
         campo_matricula.set valor_matricula 
   end

   def valida_mensagem_alteracao_sucesso(mensagem_alteracao)
       page.assert_text(mensagem_alteracao)   
   end

   def valida_mensagem_alteracao_caracteres_invalidos(mensagem_alteracao)
       page.assert_text(mensagem_alteracao)   
   end

   def clicar_criacao_aluno_via_sistema_nominal(nome_aluno, valor_matricula, mensagem_alteracao)
      click_button("Criar novos alunos")
      find(:link, "Criar alunos via sistema").click
      voucher_nominal.click
      campo_nome_aluno.set nome_aluno
      campo_matricula.set valor_matricula
      campo_matricula.send_keys(:tab)
      botao_criar_alunos.click
      page.assert_text(mensagem_alteracao)
   end

   def clicar_criacao_aluno_via_sistema_caracteres_especiais(valor_matricula, mensagem_alteracao)
      click_button("Criar novos alunos")
      find(:link, "Criar alunos via sistema").click
      voucher_nominal.click
      campo_matricula.set valor_matricula
      page.assert_text(mensagem_alteracao)
      campo_matricula.send_keys(:tab)
   end

end


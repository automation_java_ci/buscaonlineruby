class Embarque < SitePrism::Page
  element :novo_cadastro, 'a[routerlink="/embarque"]'
  element :codigo_acesso, '#voucher'
  element :prosseguir, 'input[value="Prosseguir"]'
  element :confirmar_dados, 'button[class="btn"]'
  element :nome, '#nome'
  element :email, '#email'
  element :confirma_email, 'input[name="confirmaEmail"]'
  element :aceita_termos, 'div.box-perfil.terms > label > span'
  element :senha, '#password'
  element :confirma_senha, '#password-confirm'
  element :nome_aluno, 'input[name="name"]'
  element :email_aluno, 'input[name="login"]'
  element :senha_aluno, '#password'
  element :confirma_senha_aluno, '#password-confirm'
  element :acesso_fb, 'a.btn-social.btn-facebook'
  element :login_fb, '#email'
  element :senha_fb, '#pass'
  element :entrar_fb, '#loginbutton'
  element :acesso_google, 'a.btn-social.btn-gmail > i'
  element :login_google, '#identifierId'
  element :proximo_google, '#identifierNext'
  element :proximo_google2, '#passwordNext > content > span'
  element :senha_google, 'input[type="password"]'
  element :cpf_professor, '#cpf'
  element :professor, 'div.box-perfil-professor > div > label'
  element :fundamental_dois, 'div.box-perfil-professor > div:nth-child(5) > div:nth-child(1) > label'
  element :selecionar_disciplinas, '.box-opcoes div:nth-child(1) a[href="javascript:void(0)"]'
  element :matematica, 'div.modal-content.modal-disciplinas > form > div:nth-child(1) > label'
  element :selecionar, 'div.modal-footer'
  element :salvar, 'button[type="submit"]'

  def logar_facebook(user_fb)
    acesso_fb.click
    login_fb.set user_fb.login_fb
    senha_fb.set user_fb.senha_fb
    entrar_fb.click
  end

  def logar_google(user_google)
    acesso_google.click
    login_google.set user_google.login_google
    proximo_google.click
    senha_google.set user_google.senha_google
    proximo_google2.click
  end

  def selecionar_professor
    wait_for_professor
    professor.click
    wait_for_fundamental_dois
    fundamental_dois.click
    wait_for_selecionar_disciplinas
    selecionar_disciplinas.click
    wait_for_matematica
    matematica.click
    selecionar.click
    salvar.click
  end
end

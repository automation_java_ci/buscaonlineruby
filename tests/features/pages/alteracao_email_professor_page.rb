class AlteracaoEmailProfessor < SitePrism::Page
    element :login, '#username'
    element :senha, '#senha'
    element :entrar, 'input[value="Entrar"]'
    element :link_dados_pessoais, 'a[href="#dados-pessoais"]'
    element :campo_email, "#email"
    element :confirma_email, 'input[placeholder="Confirmar novo e-mail"]'
    element :confirma_email_novo, 'input[name="confirmaEmail"]'
    element :btn_salvar, 'input[value="Salvar"]'
    element :campo_email_mailinator, '#inboxfield'
    element :btn_go, 'span[class="input-group-btn"]'
    element :passwd_cadastro, '#password'
    element :botao_prosseguir, 'input[type="submit"]'
    element :passwd_cadastro_confirmacao, '#password-confirm'
   

    def acessa_dados_pessoais
        visit('/')
        login.set(CONFIG['email_professor'])
        senha.set(CONFIG['senha_professor'])
        entrar.click
        link_dados_pessoais.click
    end

    def ativacao_novo_email(email_professor)
        visit "https://www.mailinator.com/"
        campo_email_mailinator.set email_professor
        btn_go.click  
        find(:xpath, "//td[@class='ng-binding' and contains(text(),'no-reply@somosid.com.br')]", match: :first).click
      
        within_frame("msg_body") do   
            find(:link, "Ativar minha conta").click
        end

        page.driver.browser.switch_to().window(page.driver.browser.window_handles.last)
      
    end

    def realiza_novo_login
        find(:link, "Entendi e quero fazer o login").click
        page.evaluate_script("window.location.reload()")
        login.set(CONFIG['email_professor_novo'])
        senha.set(CONFIG['senha_professor_novo'])
        entrar.click
        wait_until_link_dados_pessoais_visible
        link_dados_pessoais.click
    end


      
    def alteracao_email_incorreto(email_professor)
        campo_email.set ""
        campo_email.send_keys(email_professor)
        confirma_email.send_keys(email_professor)
    end

    def alteracao_email(email_professor)
        campo_email.set ""
        campo_email.send_keys(email_professor)
        confirma_email.send_keys(email_professor)
        btn_salvar.click
    end
    
end


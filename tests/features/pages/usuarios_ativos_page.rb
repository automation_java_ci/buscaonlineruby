class UsuariosAtivos < SitePrism::Page
  element :login, '#username'
  element :senha, '#senha'
  element :entrar, 'input[value="Entrar"]'
  element :voucher, '#ChaveCupom'
  element :salvar, 'input[type="submit"]'
  element :salvar_dados, 'button[type="submit"]'
  element :esqueci_senha, 'div.box-links.clearfix > a'
  element :email, '#email'
  element :enviar, 'input[value="Enviar"]'
  element :dados_pessoais, 'a[href*="dados-pessoais"]'
  element :nome, '#name'
  element :email, '#email'
  element :data_nascimento, '#dataNascimento'
  element :dados_escolares, 'a[href*="#dados-escolares"]'
  element :escolher_escola, 'input[value="Escolha sua escola"]'
  element :cpf, '#cpf'
  element :ddd, '#area'
  element :telefone, '#telefone'
  element :tamandare, 'li:nth-child(3) > span'
  element :professor, 'div.box-perfil-professor > div.box-perfil > label'
  element :educacao_infantil, 'div[data-index="1"]'
  element :selecionar_disciplinas, '.box-opcoes div:nth-child(1) a[href="javascript:void(0)"]'
  element :material_basico, 'div.modal-content.modal-disciplinas > form > div:nth-child(1) > label'
  element :selecionar, 'div.modal-footer'

  def minha_conta(user)
    dados_pessoais.click
    nome.set user.nome
    data_nascimento.native.clear
    data_nascimento.click
    data_nascimento.set user.data_nascimento
  end

end
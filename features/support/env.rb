require 'capybara/cucumber'
require 'selenium-webdriver'
require 'rspec'
require 'site_prism'       
require 'page-object'
require 'data_magic'
require_relative 'helper.rb'
require_relative 'page_helper.rb'

World(Helper)
World(PageObjects)

BROWSER = ENV['BROWSER']

Capybara.register_driver :selenium do |app|

    if BROWSER.eql?('chrome')
    Capybara::Selenium::Driver.new(app, :browser => :chrome)
    options = ::Selenium::WebDriver::Chrome::Options.new
    options.args << '--start-maximized'
    options.args << '--disable-infobars'

    Capybara::Selenium::Driver.new(app, browser: :chrome, options: options) 

    elsif BROWSER.eql?('chrome_headless')
        Capybara::Selenium::Driver.new(app, :browser => :chrome, 
        desired_capabilities: Selenium::WebDriver::Remote::Capabilities.chrome(
            'chromeOptions' => {'args' => ['--headless', 'disable-gpu','--start-maximized','--no-sandbox','--disable-dev-shm-usage','--window-size=1600,1024']}
            )
        )
    end
end

Capybara.configure do |conf|
    conf.default_driver= :selenium
    conf.app_host = "https://www.google.com.br/"
end
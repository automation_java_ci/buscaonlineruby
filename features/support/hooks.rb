After do |scenario|
  scenario_name = scenario.name.gsub(/\s+/, '_').tr('/', '_')
  scenario_name = scenario_name.gsub(',', '')
  scenario_name = scenario_name.gsub('(', '')
  scenario_name = scenario_name.gsub(')', '')
  scenario_name = scenario_name.gsub('#', '')
  
  if scenario.failed?
    tirar_foto(scenario_name.downcase!, 'falhou')
  else
    tirar_foto(scenario_name.downcase!, 'passou')
  end
end
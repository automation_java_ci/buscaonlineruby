#language: pt

Funcionalidade: Realizar uma busca por um site de busca
    Eu como usuário desejo acessar o buscador
    Para realizar pesquisas

@realiza_busca
Esquema do Cenário: Acesso ao buscador
    Dado que acesso a url do google
    E que pesquiso pelo "<informacao>"
    Quando clico no primeiro link da busca que tenha "<resultado>"
    Então valido a "<titulo>" da pagina clicada

  Exemplos:
      |informacao               | resultado                               | titulo                        |
      |Ruby capybara            | Capybara (software) - Wikipedia         | Capybara (software)           |
      |BDD Cucumber Wiki        | Behavior-driven development - Wikipedia | Behavior-driven development   |



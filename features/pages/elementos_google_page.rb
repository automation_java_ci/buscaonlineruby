class ElementosGoogle < SitePrism::Page
    set_url 'search'    
    element :campo_busca, ".gLFyf.gsfi"
    element :titulo_pagina, "#firstHeading"
        
    def digita_dados(informacao)
        campo_busca.set informacao    
        campo_busca.send_keys(:enter)
    end

    def clica_link(resultado)
        find(:link, resultado).click    
    end

    def validacao_titulo
        titulo_pagina.text 
    end

end
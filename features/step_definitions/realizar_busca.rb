Dado("que acesso a url do google") do
    home.load
end

Quando("que pesquiso pelo {string}") do |informacao|
  home.digita_dados(informacao)
end

Quando("clico no primeiro link da busca que tenha {string}") do |resultado|
  home.clica_link(resultado)
end

Então("valido a {string} da pagina clicada") do |titulo|
  @titulo_pg = home.validacao_titulo
  expect(@titulo_pg).to eq titulo
end